This is the answer to the technical exam of Amaysim!

Pre-requisites
1. Maven should be installed and configured in the local machine. ie. %MAVEN_HOME%\bin folder is added to the environment variables

Steps how to run the project via command line
1. Download the project in https://gitlab.com/allnvrgs/AmaysimExam
2. Extract the project
3. Download the geckodriver here: https://github.com/mozilla/geckodriver/releases
4. Extract the driver and and copy the absolute path
5. Open the config.properties file located in the project root directory and update the GECKODRIVER_ABSOLUTEPATH
6. Open command line and navigate to the project root directory where the pom.xml is 
7. Type the command: maven clean install
This should run the download all the dependencies in pom.xml and will run the test. If it didn't run the tests, type: maven test
8. When the test execution is done, open the extent.html located in the project root directory to see the report

Steps how to run the project via eclipse
1. Download the project in https://gitlab.com/allnvrgs/AmaysimExam
2. Extract the project
3. Download the geckodriver here: https://github.com/mozilla/geckodriver/releases
4. Extract the driver and and copy the absolute path
5. Open the config.properties file located in the project root directory and update the GECKODRIVER_ABSOLUTEPATH
6. Open the project in eclipse
7. Right click on the project root directory and go to Maven > Update Project
This should download all the dependencies in the pom.xml
8. After the download is done, right click on the project root directory and go to Run As > maven test
9. When the test execution is done, open the extent.html located in the project root directory to see the report