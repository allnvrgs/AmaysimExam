package sample.Amaysim;

import java.io.File;
import java.util.Random;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class Utilities {
	public static String generateRandom() {
		String aToZ = "1234567890";
		Random rand=new Random();
		StringBuilder res=new StringBuilder();
		for (int i = 0; i < 4; i++) {
			int randIndex=rand.nextInt(aToZ.length()); 
			res.append(aToZ.charAt(randIndex));            
		}
		return res.toString();
	}
	
	public static String captureScreenshot(WebDriver driver, String screenshotName) {
		try{
			TakesScreenshot ts = (TakesScreenshot) driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
			String dest = System.getProperty("user.dir") + "\\test-output\\" + screenshotName + ".png";
			File destination = new File(dest);
			org.apache.commons.io.FileUtils.copyFile(source, destination);
			System.out.println("Screenshot Taken!");
			System.out.println(dest);
			return dest;
			
		} catch(Exception e) {
			System.out.println("Exception while taking screenshot: " + e.getMessage());
			return e.getMessage();
		}
		
		
	}
}


