package sample.Amaysim;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ManageSettingsTest extends BaseClass {

	@Test
	public void UpdateSimNicknameTest() throws InterruptedException {

		initializeTestIdAndCategory("UpdateSimNicknameTest", "Manage Settings");

		loginClass.login(driver, "0468827174", "theHoff34");
		String rand = Utilities.generateRandom();
		manageSettingsPage.updateSimNickName(driver, "Testname " + rand);

	}

	@Test
	public void UpdateRechargePINTest() throws InterruptedException {

		initializeTestIdAndCategory("UpdateRechargePINTest", "Manage Settings");

		loginClass.login(driver, "0468827174", "theHoff34");
		String rand = Utilities.generateRandom();
		manageSettingsPage.updateRechargePIN(driver, rand);

	}

	@Test
	public void SampleFailedTest() {
		initializeTestIdAndCategory("Sample Failed Test", "Manage Settings");

		Assert.assertTrue(false);
	}









}
