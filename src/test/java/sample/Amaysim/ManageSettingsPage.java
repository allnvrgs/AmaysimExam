package sample.Amaysim;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class ManageSettingsPage {
	public void updateSimNickName(WebDriver driver, String simNickName) throws InterruptedException {
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id='menu_list']/li[8]/a/span[1]")).click();
		driver.findElement(By.xpath("//*[@id='edit_settings_phone_label']")).click();

		WebElement editSimNickNameField = driver.findElement(By.xpath("//*[@id='my_amaysim2_setting_phone_label']"));
		editSimNickNameField.clear();
		editSimNickNameField.sendKeys(simNickName);
		driver.findElement(By.xpath("//*[@id='edit_settings_sim_nickname']/div[3]/div/input")).click();
		Thread.sleep(3000);

		WebElement simNickname = driver.findElement(By.xpath("//*[@id='settings_sim_nickname']/div/div[1]/div/div[2]"));

		Assert.assertEquals(simNickname.getText(), simNickName);
	}
	
	public void updateVoiceMail(WebDriver driver) throws InterruptedException {
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id='menu_list']/li[8]/a/span[1]")).click();
	}
	
	public void updateRechargePIN(WebDriver driver, String newPIN) throws InterruptedException {
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id='menu_list']/li[8]/a/span[1]")).click();
		driver.findElement(By.xpath("//*[@id='edit_settings_recharge_pin']")).click();

		WebElement editPINField = driver.findElement(By.xpath("//*[@id='my_amaysim2_setting_topup_pw']"));
		editPINField.clear();
		editPINField.sendKeys(newPIN);
		driver.findElement(By.xpath("//*[@id='edit_settings_topup_password']/div[3]/div/input")).click();
		
		Thread.sleep(3000);

		WebElement actualPIN = driver.findElement(By.xpath("//*[@id='settings_recharge_pin']/div/div/div[1]/div/div[2]"));

		Assert.assertEquals(actualPIN.getText(), newPIN);
	}
}
