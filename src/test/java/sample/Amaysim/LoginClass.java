package sample.Amaysim;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class LoginClass {
	
	public void login(WebDriver driver, String username, String password) throws InterruptedException {
		
		driver.findElement(By.xpath("//*[@id='block-useraccountmenu']/ul/li[2]/a/span")).click();
		driver.findElement(By.xpath("//*[@id='username']")).sendKeys(username);
		driver.findElement(By.xpath("//*[@id='password']")).sendKeys(password);
		driver.findElement(By.xpath("//*[@id='new_session']/input[4]")).click();
		Thread.sleep(30000);

		WebElement welcomeMessage = driver.findElement(By.xpath("//*[@id='welcome_popup']/h1"));
		Assert.assertEquals(welcomeMessage.getText(), "Welcome to My amaysim");

		driver.findElement(By.xpath("//*[@id='welcome_popup']/a")).click();
	}
}
