package sample.Amaysim;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class BaseClass {

	public WebDriver driver;
	Properties property;
	FileInputStream fs;

	LoginClass loginClass = new LoginClass();
	ManageSettingsPage manageSettingsPage = new ManageSettingsPage();

	public static ExtentReports extent = new ExtentReports();
	public static ExtentTest test;



	@BeforeMethod
	public void setUp() throws IOException {
		String geckodriver = getGeckoDriverAbolutePath();
		System.setProperty("webdriver.gecko.driver", geckodriver);
		driver = new FirefoxDriver();
		String STARTING_URL = getEnvironment();
		driver.get(STARTING_URL);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();	

		ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter("extent.html");
		extent.attachReporter(htmlReporter);

	}

	@AfterMethod
	protected void afterMethod(ITestResult result, ITestContext ctx) throws IOException {
		//		System.out.println(ctx.getCurrentXmlTest().getParameter("id"));
		if (result.getStatus() == ITestResult.FAILURE) {
			String dest = Utilities.captureScreenshot(driver, result.getMethod().getMethodName());
			test.fail(result.getThrowable()).addScreenCaptureFromPath(dest); 
		} else if (result.getStatus() == ITestResult.SKIP) {
			test.skip("Test skipped " + result.getThrowable());
		} else {
			test.pass("Test Passed");
		}
		extent.flush();
		driver.quit();
	}

	public static void initializeTestIdAndCategory(String id, String category) {
		test = extent.createTest(id).assignCategory(category);
	}

	public String getEnvironment() throws IOException {
		
		fs = new FileInputStream(System.getProperty("user.dir")+"\\config.properties");
		property = new Properties();
		property.load(fs);
		String env = property.getProperty("STARTING_URL");
		return env;
	}
	
	public String getGeckoDriverAbolutePath() throws IOException {
		
		fs = new FileInputStream(System.getProperty("user.dir")+"\\config.properties");
		property = new Properties();
		property.load(fs);
		String path = property.getProperty("GECKODRIVER_ABSOLUTEPATH");
		return path;
	}

}
